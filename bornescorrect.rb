require "highline/import"

class Deck
	attr_accessor :cards

	def initialize()	
		descriptions = [
		["ACCIDENT",3,"ATTAQUE","Accident!"],
		["PANNE",3,"ATTAQUE","En panne !"],
		["CREVAISON",3,"ATTAQUE",""],
		["LIMIT_VIT",4,"ATTAQUE",""],
		["FEU_ROUGE",5,"ATTAQUE",""],
		["REPARATION",6,"DEFENSE",""],
		["ESSENCE",6,"DEFENSE",""],
		["ROUE_SEC",6,"DEFENSE",""],
		["LIMIT_FIN",6,"DEFENSE",""],
		["FEU_VERT",14,"DEFENSE",""],
		["AS_DU_VOLANT",1,"BOTTE",""],
		["INCREVABLE",1,"BOTTE",""],
		["CITERNE",1,"BOTTE",""],
		["PRIORITAIRE",1,"BOTTE",""],
		["25",10,"DISTANCE",""],
		["50",10,"DISTANCE",""],
		["75",10,"DISTANCE",""],
		["100",12,"DISTANCE",""],
		["200",4,"DISTANCE",""]]
		puts "New deck created..."
		@cards=[]
		
		#Generating deck :
		descriptions.each  do |description|
			for i in 1..description[1] do
				une_carte=Carte.new
				une_carte.libelle=description[0]
				une_carte.type=description[2]
				add_card(une_carte)
			end
		end 
		#calling self method "melange" 
		melange
    end
    
	def add_card(card)
		#append 'card' object to 'cards' array.
		@cards << card
	end 
	
	def melange()
		puts "Cards shuffled..."
		@cards.shuffle!
	end
	
	def afficher_jeu()			
	
		for i in 1..(@cards.count) do
			puts "#{@cards[i-1].libelle} -> #{@cards[i-1].state}"
		end 
	end
	
	def get_libelle(i)
		return @cards[i].libelle
	end
	
	def get_type(i)
		return @cards[i].type
	end
	
	def tirer_carte()
		# point on the last card (0)
		i=0
		while i<@cards.count && @cards[i].state != "FREE" do
			i=i+1
		end
		if (i==(@cards.count)) then
			puts "plus de carte !!!"
			# ICI, on va reseter les DROPPED -> FREE
		else 
			@cards[i].state = "TAKEN"
			return i	
		end
		
	end
	
	def defausser(id_carte)
		@cards[id_carte].state = "DROPPED"
	end
	
	def jouer(id_carte)
		@cards[id_carte].state = "PLAYED"
	end
	
end

class Player
	attr_accessor :name, :hand, :distance
	def initialize(*args)	
		@hand = []
		@name = args[0]
		@distance=0
		puts "Cards for #{self.name} ="
		for i in 1..6 do
			@hand << $mon_deck.tirer_carte	
			puts $mon_deck.cards[@hand[i-1]].libelle
		end
	end
	
	def pioche()
		@hand << $mon_deck.tirer_carte	
	end

	def can_play()
		can_play = false
		for i in 0..(@hand.count-1) do
			if ($mon_deck.get_libelle(@hand[i])=="FEU_VERT") then
				can_play=true
			end
		end
		return can_play
	end
	
	def play()
		pioche()
		if can_play()==false then
			ask_defausse()
		else
			ask_play()
		end 
	end
	
	def afficher_main()
		for i in 0..(@hand.count-1) do
			puts "#{i+1} - #{$mon_deck.get_libelle(@hand[i])}"
		end
	
	end
	
	def ask_play()
		puts "JOUER :"
		afficher_main()
		input = ask "-> Carte à jouer : "
		id_carte_jouer= @hand[(input.to_i-1)]
		$mon_deck.jouer(id_carte_jouer)
		
		# Calculate Distance. 
		if ($mon_deck.get_type(@hand[input.to_i-1])=="DISTANCE") then
			@distance=@distance+$mon_deck.get_libelle(@hand[input.to_i-1]).to_i
		end
		
		@hand.delete_at(input.to_i-1)
		
	end 
	
	def ask_defausse()
		puts "DEFAUSSER :"
		afficher_main()
		input = ask "-> Carte à defausser : "
		id_carte_defausser = @hand[(input.to_i-1)]
		$mon_deck.defausser(id_carte_defausser)
		
		@hand.delete_at(input.to_i-1)

	end

end

class Carte 
	attr_accessor :libelle, :type, :state
	#state = FREE / PLAYED / DROPPED / TAKEN
	def initialize()
		@state='FREE'
	end
end

$mon_deck = Deck.new

#  Initialisation 
playerA = Player.new("Joueur A")
playerB = Player.new("Joueur B")
playerC = Player.new("Joueur C")
playerD = Player.new("Joueur D")

# On commence à jouer 
while true do
	puts "JOUEUR A (#{playerA.distance})"
	playerA.play()
	$mon_deck.afficher_jeu()
	puts "JOUEUR B (#{playerB.distance})"
	playerB.play()
	$mon_deck.afficher_jeu()
end
