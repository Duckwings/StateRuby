require 'state_machine'
require "highline/import"
class PopVehicle
	state_machine :initial => :parked do

		event :park do
			transition [:idling] => :parked
		end

		event :ignite do
			transition [:parked] => :idling
		end

		event :gear_up do
			transition [:idling] => :first_gear,
				   [:first_gear] => :second_gear,
				   [:second_gear] => :third_gear,
				   [:third_gear] => :crashed
		end

		event :gear_down do
			transition [:third_gear] => :second_gear,
				   [:second_gear] => :first_gear,
				   [:first_gear] => :idling
		end

		event :crashed do
			transition [:first_gear, :second_gear, :third_gear] => :crashed
		end

		event :repair do
			transition [:crashed] => :parked
		end
	end
end

vehicle = PopVehicle.new
vehicle2 = PopVehicle.new

while true do
	puts "Le véhicule 1 est : #{vehicle.state}"
	puts "et peut recevoir : #{vehicle.state_events}"
	input = ask "~> action 1 ?:"
	vehicle.send(input)
	puts "Le véhicule 2 est : #{vehicle2.state}"
	puts "et peut recevoir : #{vehicle2.state_events}"
	input = ask "~> action 2 ?:"
	vehicle2.send(input)
end
